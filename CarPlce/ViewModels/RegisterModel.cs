﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarPlce.ViewModels
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Не указан Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Не указан повтор пароль")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароль введен неверно")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Не указан Имя")]
        public string Firstname { get; set; } // имя
        
        [Required(ErrorMessage = "Не указан Фамилия")]
        public string Lastname { get; set; } // фамилия
        
        [Required(ErrorMessage = "Не указан Дата рождения")]
        public DateTime DOB { get; set; } // Дата рождения

        [Required(ErrorMessage = "Не указан Адрес")]
        public string Address { get; set; } // Адрес

        [Required(ErrorMessage = "Не указан Номер телефона")]
        public string Phone { get; set; } // Номер телефона
    }
}
