﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarPlce.ViewModels
{
    public class UserAvatarModel
    {
        public IFormFile Photo { get; set; }  

        [Required(ErrorMessage = "Не указан Email")]
        public string Email { get; set; } // Эдектронный адрес

        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; } // Пароль

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароль введен неверно")]
        public string ConfirmPassword { get; set; } // Повтор пароля

        [Required]
        public string Firstname { get; set; } // имя 
        [Required]
        public string Lastname { get; set; } // фамилия 
        [Required]
        public DateTime DOB { get; set; } // Дата рождения
        [Required]
        public string Address { get; set; } // Адрес
        [Required]
        public string Phone { get; set; } // Номер телефона
    }
}
