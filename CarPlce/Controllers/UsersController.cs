﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CarPlce.Models;
using CarPlce.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CarPlce.Controllers
{
    public class UsersController : Controller
    {
        private UsersContext db;
        private IWebHostEnvironment hostingEnvironment;
        public UsersController(UsersContext context, IWebHostEnvironment hostingEnvironment)
        {
            db = context;
            this.hostingEnvironment = hostingEnvironment;
        }
        [Authorize]
        // GET: User
        public ActionResult Index()
        {
            ViewBag.users = db.Users.ToList();
            //ViewBag.filemodel = db.Files.ToList();
            return View();
        }
        public ActionResult Index1()
        {
            ViewBag.users = db.Users.ToList();
            return View();
        }
        [HttpGet] // метод добавления Avatar
        public ActionResult AddFile()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddFile(UserAvatarModel model)
        {
            if (ModelState.IsValid)
            {
                string uniqueFileName = null;
                if(model.Photo != null)
                {
                    string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "img");
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Photo.FileName;
                    string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                    model.Photo.CopyTo(new FileStream(filePath, FileMode.Create));
                }
                
                User user = new User //конструктор UserAvatarModel
                { 
                    Email = model.Email,
                    Password = model.Password,
                    Address = model.Address,
                    Firstname = model.Firstname,
                    Lastname = model.Lastname,
                    DOB = model.DOB,
                    Phone = model.Phone,
                    PhotoPath = uniqueFileName
                };
                db.Users.Add(user);
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpGet] // метод добавления
        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add( User user)
        {
            if (ModelState.IsValid)
            {
                // добавляем информацию в базу данных
                db.Users.Add(user);
                // сохраняем в бд все изменения
                db.SaveChanges();
                return RedirectToAction("Index");
            }  
            else
              return View(user);
        }
        [HttpGet] // метод редактирования
        public async Task<IActionResult> Edit(int? id)
        {
            if (id != null)
            {
                User user = await db.Users.FirstOrDefaultAsync(p => p.Id == id);
                if (user != null)
                    return View(user);
            }
            return NotFound();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit( User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Update(user);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            } 
            else
              return View(user);
        }
        [HttpGet] //метод удаления
        [ActionName("Delete")]
        public async Task<IActionResult> ConfirmDelete(int? id)
        {
            if (id != null)
            {
                User user = await db.Users.FirstOrDefaultAsync(p => p.Id == id);
                if (user != null)
                    return View(user);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int? id)
        {
            {
                if (id != null)
                {
                    User user = new User { Id = id.Value };
                    db.Entry(user).State = EntityState.Deleted;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                return NotFound();
            }
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }
    }
}







/*
 * Регистрация
 * [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(User model)
        {
            if (ModelState.IsValid)
            {
                //User client = await db.Users.FirstOrDefaultAsync(u => u.Email == model.Email && u.Password == model.Password);
               // if (client != null)
               // {
                    await Authenticate(model.Email); // аутентификация

                    return RedirectToAction("Index", "Users");
               // }
               // ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(User model)
        {
            if (ModelState.IsValid)
            {
                // User client = await db.Users.FirstOrDefaultAsync(u => u.Email == model.Email);
                // if (client == null)
                // {
                // добавляем пользователя в бд
                db.Users.Add(model);    //new User { Email = model.Email, Password = model.Password });
                    await db.SaveChangesAsync();

                    await Authenticate(model.Email); // аутентификация

                    return RedirectToAction("Index", "Users");
               // }
               // else
                //    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }
*/
