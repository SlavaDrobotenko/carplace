﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using CarPlce.Models;
using CarPlce.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CarPlce.Controllers
{
    public class AccountController : Controller
    {
        private UsersContext db;
        public AccountController(UsersContext context)
        {
            db = context;
           
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost] // метод изменения пароля
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                string value = model.NewPassword;
                string newpasshashstr = NewMethod(value);
                value = model.ConfirmPassword;
                string confirmpasshashstr = NewMethod(value);
                /*
                var newsha = new SHA1CryptoServiceProvider();
                byte[] newpasshashbyte = newsha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(model.NewPassword)); // хешируем приходящий новый пароль
                var newpasshashstr = Convert.ToBase64String(newpasshashbyte);

                var confirmsha = new SHA1CryptoServiceProvider();
                byte[] confirmpasshashbyte = confirmsha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(model.ConfirmPassword)); // хешируем приходящий подтверждения нового пароля
                var confirmpasshashstr = Convert.ToBase64String(confirmpasshashbyte);
                */

                if (newpasshashstr == confirmpasshashstr) // проверка совпадения нового пароля и подтверждения пороля.
                {
                    var client = await db.Users.FindAsync(ClaimTypes.Email); // поиск в базе данных юзера по мылу , который берём из куков.
                    value = model.OldPassword;
                    string oldpasshashstr = NewMethod(value);
                    /*
                    var oldconfirmsha = new SHA1CryptoServiceProvider();
                    byte[] oldpasshashbyte = oldconfirmsha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(model.OldPassword)); // хешируем приходящий старый пароль
                    var oldpasshashstr = Convert.ToBase64String(oldpasshashbyte);*/

                    if (client != null && client.Password == oldpasshashstr)// client.Password == model.OldPassword --- проверка по поиску в базе данных юзера по мылу, который берем из куков.
                    {                                                            // client != null --- проверка по поиску в базе данных юзера по мылу, который берем из куков.
                        client.Password = newpasshashstr;                 // Обновляем пароль.
                        db.Users.Update(client);
                        await db.SaveChangesAsync();
                        return RedirectToAction("ChangePasswordConfirmation");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Пользователь не найден или Пароли не совпадают");
                    }
                }

            }
            return View(model);
        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                string value = model.Password;
                string passhashstr = NewMethod(value);
                // var sha1 = new SHA1CryptoServiceProvider();
                // byte[] passhashbyte = sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(model.Password)); // хешируем приходящий пароль
                // var passhashstr = Convert.ToBase64String(passhashbyte);

                User client = await db.Users.FirstOrDefaultAsync(u => u.Email == model.Email && u.Password == passhashstr);
                 if (client != null)
                 {
                await Authenticate(model.Email); // аутентификация

                return RedirectToAction("Index1", "Users");
                 }
                 ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                 User client = await db.Users.FirstOrDefaultAsync(u => u.Email == model.Email);
                 if (client == null)
                 {
                    string value = model.Password;
                    string passhashstr = NewMethod(value);

                    //var sha1 = new SHA1CryptoServiceProvider();
                    //byte[] passhashbyte = sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(model.Password)); // Вычисляет хэш-значение для указанного массива байтов. Обращение к полю model.Password
                    //var passhashstr = Convert.ToBase64String(passhashbyte); // метод для восстановления исходного массива байтов.

                    // добавляем пользователя в бд
                    db.Users.Add(new User { Email = model.Email, Password = passhashstr, Address = model.Address, Firstname = model.Firstname, Lastname = model.Lastname, DOB = model.DOB, Phone = model.Phone });
                await db.SaveChangesAsync();

                await Authenticate(model.Email); // аутентификация

                return RedirectToAction("Index1", "Users");
                 }
                 else
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }
        private static string NewMethod(string value)
        {
            var sha1 = new SHA1CryptoServiceProvider();
            byte[] passhashbyte = sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(value)); // Вычисляет хэш-значение для указанного массива байтов. Обращение к полю model.Password
            var values = Convert.ToBase64String(passhashbyte); // метод для восстановления исходного массива байтов.
            return values;
        }

        private async Task Authenticate(string Firstname)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, Firstname)
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        
    }
}
/*public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (model.NewPassword == model.ConfirmPassword) // проверка совпадения нового пароля и подтверждения пороля.
            {
               if (ModelState.IsValid)
               {
                  var client = await db.Users.FindAsync(ClaimTypes.Email); // поиск в базе данных юзера по мылу , который берём из куков.
                  if(client == null) // проверка по поиску в базе данных юзера по мылу, который берем из куков.
                  {
                        ModelState.AddModelError("", "Пользователь не найден");
                  }
                  else if (model.OldPassword == client.Password)         // Проверяем равен ли введенный старый пароль паролю в базе данных.
                  {
                    client.Password = model.NewPassword;                 // Обновляем пароль.
                    db.Users.Update(client);
                    await db.SaveChangesAsync();
                    return RedirectToAction("ChangePasswordConfirmation");
                  }
                  else
                  {
                    ModelState.AddModelError("", "Пользователь не найден");
                  }
                return View(model);
               }
            }
            else
            {
                ModelState.AddModelError("", "Пароли не совпадают");
            }
            return View(model);
        }
*/ 