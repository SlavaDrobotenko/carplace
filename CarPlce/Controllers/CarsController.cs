﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CarPlce.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace CarPlce.Controllers
{
    public class CarsController : Controller
    {
        private UsersContext db;
        public CarsController(UsersContext context)
        {
            db = context;
        }
        // GET: Cars
        public ActionResult Index()
        {
            // ViewBag.users = db.Users.ToList();
            // ViewBag.cars = db.Cars.ToList();
            ViewBag.cars = db.Cars.Include(r => r.User).ToList();  // Для загрузки связных данных
            return View();
        }
        [HttpGet] // метод добавления
        public ActionResult Add()
        {
            ViewBag.Users = new SelectList(db.Users, "Id", "Lastname"); //выподающий список
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(Car car)
        {
            if (ModelState.IsValid)
            {
                // добавляем информацию в базу данных
                db.Cars.Add(car);
                // сохраняем в бд все изменения
                db.SaveChanges();
                return RedirectToAction("Index");
            }
                
            else
            return View(car);
        }
        [HttpGet] // метод редактирования
        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.Users = new SelectList(db.Users, "Id", "Lastname");
            if (id != null)
            {
                Car car = await db.Cars.FirstOrDefaultAsync(p => p.Id == id);
                if (car != null)
                    return View(car);
            }
            return NotFound();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit( Car car)
        {
            if (ModelState.IsValid)
            {
                db.Cars.Update(car);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            else
            return View(car);
        }
        [HttpGet] //метод удаления
        [ActionName("Delete")]
        public async Task<IActionResult> ConfirmDelete(int? id)
        {
            if (id != null)
            {
                Car car = await db.Cars.FirstOrDefaultAsync(p => p.Id == id);
                if (car != null)
                    return View(car);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int? id)
        {
            {
                if (id != null)
                {
                    Car car = new Car { Id = id.Value };
                    db.Entry(car).State = EntityState.Deleted;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                return NotFound();
            }
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}