﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarPlce.Models
{
    public class Car
    {
        public int Id { set; get; }
        [Required(ErrorMessage = "Не указан цвет авто")]
        public string Color { set; get; } //цвет авто
        [Required(ErrorMessage = "Не указан номер")]
        public string Number { set; get; } //номер
        [Required(ErrorMessage = "Не указан (date of create) - дата создания")]
        public DateTime Doc { set; get; } // (date of create) - дата создания
        [Required(ErrorMessage = "Не указан марка авто")]
        public string Mark { set; get; } // марка авто
        [Required(ErrorMessage = "Не указан модель авто")]
        public string Model { set; get; } // модель авто
        public int UserId { get; set; }  // Это свойство будет использоваться как внешний ключ
        public User User { get; set; } // связь один ко многим
    }
}
