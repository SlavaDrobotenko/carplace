﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPlce.Models
{
    public class UsersContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Car> Cars { get; set; }
        public UsersContext(DbContextOptions<UsersContext> options)
               : base(options)
        {
            Database.EnsureCreated();
        }
        // Связь один ко многим (one to many)
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Car>()
                .HasOne(p => p.User)
                .WithMany(t => t.Cars)
                .HasForeignKey(p => p.UserId);
        }
    }
}
